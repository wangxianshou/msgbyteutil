package sousou.demo;

import sousou.define.IEnumGetter;
import sousou.define.Setting;

public enum DemoByteFormat implements IEnumGetter{

	COMMAND(0,"",
			null,
			new Setting(Setting.BYTE_TYPE_C ,4,8,"1001")
			),
	RESULT_CODE(1,"",
			null,
			new Setting(Setting.BYTE_TYPE_C ,4,8,"")
			),
	INFO1_LEN(0,"",
			null,
			new Setting(Setting.BYTE_TYPE_H ,4,8,"")
			),
	INFO1(0,"INFO1_LEN",
			null,
			new Setting(Setting.BYTE_TYPE_C ,-1,-1,"")
			),
	INFO2_LEN(0,"",
			null,
			new Setting(Setting.BYTE_TYPE_H ,4,8,"")
			),
	INFO2(0,"INFO2_LEN",
			null,
			new Setting(Setting.BYTE_TYPE_C ,-1,-1,"")
			),
	;

	private final int idx;
	private final String depender;
	private final Setting keyInfo;
	private final Setting valInfo;

	DemoByteFormat(final int idx,final String depender,final Setting keyInfo,final Setting valInfo){
		this.idx = idx;
		this.depender = depender;
		this.keyInfo = keyInfo;
		this.valInfo = valInfo;

	}

	@Override
	public int getIdx() {
		// TODO 自動生成されたメソッド・スタブ
		return this.idx;
	}

	@Override
	public String getDepender() {
		// TODO 自動生成されたメソッド・スタブ
		return this.depender;
	}

	@Override
	public Setting getKeyInfo() {
		// TODO 自動生成されたメソッド・スタブ
		return this.keyInfo;
	}

	@Override
	public Setting getValInfo() {
		// TODO 自動生成されたメソッド・スタブ
		return this.valInfo;
	}

	@Override
	public int getStandLen() {
		// TODO 自動生成されたメソッド・スタブ
		return 0;
	}

}
