package sousou.api;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import sousou.define.EnumGenerics;
import sousou.define.Setting;
import sousou.utils.ByteUtil;
import sousou.utils.StringUtil;

public class MessageApi {

	/**
	 * 電文データ作成
	 * @param format          電文定義(Enum)
	 * @param data　　　　　　　　　電文明細データ
	 * @param isAddKey        keyいるか(true:要 false:不要)
	 * @param nullIfDefault   valはnullの場合、ディフォルト値入替か(true:要 false:不要)
	 * @return　　　　　　　　　　　　バイト配列
	 * @throws Exception
	 */
	public static <T extends Enum<T>> byte[] bytesContactSingle(
			Class<T> format,
			LinkedHashMap<Enum<T>,byte[]> data,
			boolean isAddKey,
			boolean nullIfDefault
			) throws Exception {
		byte[] result = null;
		List<byte[]> bytesList = new ArrayList<>();
		boolean needAdd = true;
		String dependerName = "";
		for(Map.Entry<Enum<T>,byte[]> item : data.entrySet()) {
			Setting keyInfo = (Setting)format.getMethod("getKeyInfo").invoke(item.getKey());
			Setting valInfo = (Setting)format.getMethod("getValInfo").invoke(item.getKey());
			dependerName = (String)format.getMethod("getDepender").invoke(item.getKey());
			//TODO 設定前提の依存処理、未実装
			if(!StringUtil.isEmpty(dependerName)) {
				Enum<T> dependerEnum = Enum.valueOf(format, dependerName);
			}
			if(!needAdd) {
				continue;
			}
			//key
			if(isAddKey) {
				bytesList.add(keyInfo.getBytes());
			}
			//value
			if(item.getValue() == null && nullIfDefault) {
				bytesList.add(valInfo.getBytes());
			}else {
				bytesList.add(item.getValue());
			}
		}
		result = ByteUtil.addBytes(bytesList.toArray(new byte[bytesList.size()][]));
		return result;
	}

	/**
	 * 電文データ作成
	 * @param data
	 * @return
	 */
	public static <T extends Enum<T>> byte[] bytesContactMulti(
			LinkedHashMap<Integer, LinkedHashMap<Enum<T>,byte[]>> data
			) {
		byte[] result = null;
		List<byte[]> bytesList = new ArrayList<>();
		for(Map.Entry<Integer, LinkedHashMap<Enum<T>,byte[]>> mapItem : data.entrySet()) {
			for(Map.Entry<Enum<T>,byte[]> item : mapItem.getValue().entrySet()) {
				bytesList.add(item.getValue());
			}
		}
		result = ByteUtil.addBytes(bytesList.toArray(new byte[bytesList.size()][]));
		return result;
	}

	/**
	 * 電文解析
	 * @param loopCnt
	 * @param data
	 * @param format
	 * @return
	 * @throws Exception
	 */
	public static <T extends Enum<T>> LinkedHashMap<Integer, LinkedHashMap<String,byte[]>> bytesSplit(
			int loopCnt,
			byte[] data,
			Class<T> format
			) throws Exception{

		LinkedHashMap<Integer, LinkedHashMap<String,byte[]>> result = new LinkedHashMap<>();
		LinkedHashMap<String,byte[]> record = new LinkedHashMap<>();
		EnumGenerics<T> enumInstance = new EnumGenerics<>(format);
		byte[] tempBytes = null;
		int offset = 0;
		int len = 0;
		for(int i =0 ;i< loopCnt;i++) {
			//
			record.clear();
			//
			for(Enum<T> it : enumInstance.values()) {
				Setting valInfo = (Setting)format.getMethod("getValInfo").invoke(it);
				String dependerName = (String)format.getMethod("getDepender").invoke(it);
				//
				if(StringUtil.isEmpty(dependerName)) {
					len = valInfo.getSize();
				}else {
					Enum<T> depender = Enum.valueOf(format, dependerName);
					len = ByteBuffer.wrap(record.get(depender.toString())).getInt();
				}
				//
				tempBytes = new byte[len];
				System.arraycopy(data, offset, tempBytes, 0, len);
				record.put(it.toString(), tempBytes);
				offset = offset + len;
			}
			result.put(i, record);
		}
		return result;

	}

	/**
	 * 電文解析
	 * @param loopCnt
	 * @param data
	 * @param format
	 * @return
	 * @throws Exception
	 */
	public static <T extends Enum<T>> LinkedHashMap<Integer, LinkedHashMap<Enum<T>,byte[]>> bytesSplitExA(
			int loopCnt,
			byte[] data,
			Class<T> format
			) throws Exception{

		LinkedHashMap<Integer, LinkedHashMap<Enum<T>,byte[]>> result = new LinkedHashMap<>();
		LinkedHashMap<Enum<T>,byte[]> record = new LinkedHashMap<>();
		EnumGenerics<T> enumInstance = new EnumGenerics<>(format);
		byte[] tempBytes = null;
		int offset = 0;
		int len = 0;
		for(int i =0 ;i< loopCnt;i++) {
			//
			record.clear();
			//
			for(Enum<T> it : enumInstance.values()) {
				Setting valInfo = (Setting)format.getMethod("getValInfo").invoke(it);
				String dependerName = (String)format.getMethod("getDepender").invoke(it);
				//
				if(StringUtil.isEmpty(dependerName)) {
					len = valInfo.getSize();
				}else {
					Enum<T> depender = Enum.valueOf(format, dependerName);
					len = ByteBuffer.wrap(record.get(depender)).getInt();
				}
				//
				tempBytes = new byte[len];
				System.arraycopy(data, offset, tempBytes, 0, len);
				record.put(it, tempBytes);
				offset = offset + len;
			}
			result.put(i, record);
		}
		return result;

	}

	/**
	 * 電文解析
	 * @param loopCnt
	 * @param data
	 * @param format
	 * @return
	 * @throws Exception
	 */
	public static <T extends Enum<T>> LinkedHashMap<Integer, LinkedHashMap<T,byte[]>> bytesSplitExB(
			int loopCnt,
			byte[] data,
			Class<T> format
			) throws Exception{

		LinkedHashMap<Integer, LinkedHashMap<T,byte[]>> result = new LinkedHashMap<>();
		LinkedHashMap<T,byte[]> record = new LinkedHashMap<>();
		EnumGenerics<T> enumInstance = new EnumGenerics<>(format);
		byte[] tempBytes = null;
		int offset = 0;
		int len = 0;
		for(int i =0 ;i< loopCnt;i++) {
			//
			record.clear();
			//
			for(Enum<T> it : enumInstance.values()) {
				Setting valInfo = (Setting)format.getMethod("getValInfo").invoke(it);
				String dependerName = (String)format.getMethod("getDepender").invoke(it);
				//
				if(StringUtil.isEmpty(dependerName)) {
					len = valInfo.getSize();
				}else {
					Enum<T> depender = Enum.valueOf(format, dependerName);
					len = ByteBuffer.wrap(record.get((T)depender)).getInt();
				}
				//
				tempBytes = new byte[len];
				System.arraycopy(data, offset, tempBytes, 0, len);
				record.put((T)it, tempBytes);
				offset = offset + len;
			}
			result.put(i, record);
		}
		return result;

	}

}
