package sousou.define;

import java.util.EnumSet;
import java.util.Set;

public class EnumGenerics<T extends Enum<T>> {

	private Set<T> values;

	public EnumGenerics(Class<T> t) {
		values = EnumSet.allOf(t);
	}

	public Set<T> values(){
		return values;
	}

}
