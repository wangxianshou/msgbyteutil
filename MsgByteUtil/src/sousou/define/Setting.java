package sousou.define;

import java.math.BigInteger;

import sousou.utils.ByteUtil;

public class Setting {

	public static final String BYTE_TYPE_X = "X";
	public static final String BYTE_TYPE_B = "B";
	public static final String BYTE_TYPE_H = "H";
	public static final String BYTE_TYPE_C = "C";

	private String type;
	private int size;
	private int fixLen;
	private String val;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getFixLen() {
		return fixLen;
	}

	public void setFixLen(int fixLen) {
		this.fixLen = fixLen;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}

	public Setting(String type,int size,int fixLen,String val) {
		this.type = type;
		this.size = size;
		this.fixLen = fixLen;
		this.val = val;
	}

	public byte[] getBytes() {
		byte[] result = null;
		switch(this.type) {
		case Setting.BYTE_TYPE_X:
			result = ByteUtil.setTypeX(this.val, this.fixLen, this.size);
			break;
		case Setting.BYTE_TYPE_C:
			result = ByteUtil.setTypeC(this.val, this.size);
			break;
		case Setting.BYTE_TYPE_H:
			result = ByteUtil.setTypeH(this.val);
			break;
		case Setting.BYTE_TYPE_B:
			result = ByteUtil.setTypeB(Integer.parseInt(this.val), this.size);
			break;
		}
		return result;

	}

	public <T> T getCastValue(byte[] valByte,Class<T> classT) {
		String strValue = "";
		Object result = null;
		int radix = 16;
		switch(this.type) {
		case Setting.BYTE_TYPE_X:
			strValue = ByteUtil.byteHtoString(valByte);
			radix = 10;
			break;
		case Setting.BYTE_TYPE_C:
			strValue = ByteUtil.byteCtoString(valByte);
			break;
		case Setting.BYTE_TYPE_H:
		case Setting.BYTE_TYPE_B:
			strValue = ByteUtil.byteHtoString(valByte);
			break;
		}

		if(classT.getName().equals(int.class.getName())) {
			result = Integer.parseInt(strValue,radix);
		}else if(classT.getName().equals(String.class.getName())) {
			result = strValue;
		}else if(classT.getName().equals(BigInteger.class.getName())) {
			result = new BigInteger(strValue,radix);
		}

		return (T)result;

	}

}
