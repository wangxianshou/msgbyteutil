package sousou.define;

public interface IEnumGetter {
	int getIdx();
	String getDepender();
	Setting getKeyInfo();
	Setting getValInfo();
	int getStandLen();

}
