package sousou.utils;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import com.payneteasy.tlv.HexUtil;

public class ByteUtil {

	/**
	 * int型数字はバイト配列に転換（sizeは１～４まで）
	 * @param data intvalue
	 * @param size (1-4)
	 * @return
	 */
	public static byte[] intToByteArray(int data,int size) {
		byte[] result = new byte[size];
		for(int i=0,j=size-1;i<size;i++,j--) {
			result[i] =(byte)(data >> 8*j);
		}
		return result;
	}

	public static int byteArrayToInt(byte[] input) {
		int result = 0x00;
		int len = input.length;
		switch(len) {
		case 1:
			result = input[0] & 0xFF;
			break;
		case 2:
			result = input[1] & 0xFF       |
			        (input[0] & 0xFF) << 8 ;
			break;
		case 3:
			result = input[2] & 0xFF       |
			        (input[1] & 0xFF) << 8 |
	                (input[0] & 0xFF) << 16;
			break;
		case 4:
			result = input[3] & 0xFF       |
			        (input[2] & 0xFF) << 8 |
	                (input[1] & 0xFF) << 16|
                    (input[0] & 0xFF) << 24;
			break;
		}
		return result;
	}

	public static byte[] setTypeB(int data,int size) {
		byte[] result = new byte[size];
		switch(size) {
		case 4:
			result = ByteBuffer.allocate(size).putInt(data).array();
			break;
		case 1:
		case 2:
		case 3:
			result = intToByteArray(data, size);
			break;
		default:
			break;
		}
		return result;
	}

	public static byte[] setTypeH(String input) {
		return HexUtil.parseHex(input);
	}

	public static String byteHtoString(byte[] input) {
		return HexUtil.toHexString(input);

	}

	public static byte[] setTypeX(String input,int digitsNum,int size) {
		byte[] result = null;
		if(size*2 < digitsNum) {
			return result;
		}
		if(input.length() > digitsNum) {
			return result;
		}

		String strF = StringUtil.repeat("F", size*2 - digitsNum);
		String str0 = StringUtil.repeat("0", digitsNum - input.length());
		result = HexUtil.parseHex(strF.concat(str0).concat(input));

		return result;
	}

	public static byte[] setTypeC(String input,int size) {
		byte[] result = null;
		if(input == null)
			return result;
		byte[] midByte = new byte[0];
		try {
			midByte = input.getBytes("Shift_JIS");
		} catch (UnsupportedEncodingException e) {
			return result;
		}
		if(midByte.length > size) {
			result = midByte;
		}else {
			result = new byte[size];
			System.arraycopy(midByte, 0, result, 0, midByte.length);
		}
		return result;
	}

	public static String byteCtoString(byte[] input) {
		try {
			return new String(input,"Shift_JIS");
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}

	public static byte[] addBytes(byte[]... input) {
		int size = 0;
		for(byte[] one : input) {
			if(one != null) {
				size += one.length;
			}
		}
		byte[] result = new byte[size];
		int index = 0;
		for(byte[] one : input) {
			if(one != null) {
				System.arraycopy(one, 0, result, index, one.length);
				index += one.length;
			}
		}

		return result;

	}

	public static int searchByteLenUntilNull(byte[] input,int offset) {
		int len = 0;
		int byteLen = input.length;
		for(int i=offset;i<byteLen;i++) {
			len++;
			if(input[i] == 0x00) {
				break;
			}
		}
		return len;
	}

}
