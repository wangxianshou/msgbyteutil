package sousou.utils;

public class StringUtil {

	public static boolean isEmpty(String input) {
		return (input == null || input.length() == 0 );
	}

	public static String repeat(String input,int count) {
		return new String(new char[count]).replace("\0", input);
	}

	public static <T> T coalesce(T... input) {
		T result = null;
		int size = input.length;
		for(int i = 0 ;i < size; i++) {
			T one = input[i];
			if(one == null) {
				continue;
			}else if(String.class.isInstance(one)) {
				if(one.equals("")) {
					if(i < size -1) {
						continue;
					}
				}
			}
			result = one;
			break;
		}
		return result;
	}

}
